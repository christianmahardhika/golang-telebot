package telebot

func ExtractChat(response ResponseTextMessage) (ResponseChat *Chat, err error) {
	ResponseChat = &response.Result[len(response.Result)-1].Message.Message.Chat.Chat
	return ResponseChat, nil
}

func ExtractMessage(response ResponseTextMessage) (ResponseMessage *Message, err error) {
	ResponseMessage = &response.Result[len(response.Result)-1].Message.Message
	return ResponseMessage, nil
}
