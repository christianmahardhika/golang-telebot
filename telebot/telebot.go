package telebot

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

var ApiTelegram = "https://api.telegram.org/bot"

type telebotInterface interface {
	SetTelegramToken(token string)
	GetUpdates() (resp *ResponseTextMessage, err error)
	SendMessage(chatId int, message string) (resp *SendMessageResult, err error)
	SendImage(chatId string, message string) (resp *ResponseTextMessage, err error)
	SetHttpProxy(urlHost string)
}

func SetTelegramToken(token string) {
	ApiTelegram = ApiTelegram + token
}

func GetUpdates() (resp *ResponseTextMessage, err error) {
	telegramUrl := ApiTelegram + "/getUpdates?timeout=100"
	response, err := http.Get(telegramUrl)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	var result ResponseTextMessage
	if err := json.Unmarshal(bodyBytes, &result); err != nil { // Parse []byte to the go struct pointer
		log.Fatal("Can not unmarshal JSON")
	}
	if result.Ok == true && len(result.Result) >= 1 {
		return &result, nil
	}
	if result.Ok == false {
		log.Fatal("bot token may be invalid")
		return nil, errors.New("bot token may be invalid")
	}
	return
}

func SendMessage(chatId int, message string) (resp *SendMessageResult, err error) {
	telegramUrl := ApiTelegram + "/sendMessage?text=" + message + "&chat_id=" + strconv.Itoa(chatId) + "&parse_mode=html"
	response, err := http.Get(telegramUrl)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	var result SendMessageResult
	if err := json.Unmarshal(bodyBytes, &result); err != nil { // Parse []byte to the go struct pointer
		log.Fatal("Can not unmarshal JSON")
	}
	return &result, nil
}

func SendImage(chatId string, message string) (resp *ResponseTextMessage, err error) {
	telegramUrl := ApiTelegram + "/sendPhoto?text=" + message + "&chat_id=" + chatId + "&parse_mode=html"
	response, err := http.Get(telegramUrl)
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var result ResponseTextMessage
	if err := json.Unmarshal(bodyBytes, &result); err != nil { // Parse []byte to the go struct pointer
		fmt.Println("Can not unmarshal JSON")
	}

	return &result, nil
}

func SetHttpProxy(urlHost string) {
	proxyUrl, err := url.Parse(urlHost)
	if err != nil {
		log.Fatal(err)
	}
	response := http.ProxyURL(proxyUrl)
	log.Println("Proxy Implemented: ", response)
}
