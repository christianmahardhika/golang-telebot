package main

import (
	"encoding/json"
	"os"

	"gitlab.com/christianmahardhika/golang-telebot/telebot"
)

func main() {
	telebot.SetTelegramToken(os.Getenv("TELEGRAM_TOKEN"))
	response, err := telebot.GetUpdates()
	if err != nil {
		print(err)
	}
	printChat, err := telebot.ExtractChat(*response)
	printMessage, err := telebot.ExtractMessage(*response)
	responseMessage, err := telebot.SendMessage(printChat.ID, string(printMessage.Text))
	printJson, err := json.Marshal(responseMessage)
	println(string(printJson))
}
